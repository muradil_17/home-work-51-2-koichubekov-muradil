import React from 'react';
import './App.css';
import Header from "./componenty/header";
import Leftblock from "./componenty/leftblock";
import Centralblock from "./componenty/centralblock";
import boots from './images/nike.webp'
import real from './images/real marid costum.jpg'
import gucci from './images/guccimen.jpg'
import classic from './images/classicstylemen.webp'
import Rightblock from "./componenty/rightblock";
import Footer from "./componenty/footer";


function App() {
  return (
    <div className="App">
        <Header Logotext="AliDordoy"/>
        <div className="big_block">
            <Leftblock/>
            <Centralblock img={boots} title="Nike CR7 boots"/>
            <Centralblock img={real} title="Real Madrid sport costume"/>
            <Centralblock img={gucci} title="T-shirt Gucci"/>
            <Centralblock img={classic} title="Classic costume"/>
            <Rightblock/>
        </div>
        <Footer/>
    </div>
  );
}

export default App;
