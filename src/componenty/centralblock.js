import React from 'react';


const Centralblock = (props) => {
    return (
        <div className="cards">
            <div className="card">
                <img src={props.img} alt=""/>
                <div className="text">
                    <h1>{props.title}</h1>
                    <p>{props.text} <span>100$</span></p>
                </div>
                <div className="buy">
                    <a href="#">BUY</a>
                </div>
            </div>
        </div>
    );
};

export default Centralblock;