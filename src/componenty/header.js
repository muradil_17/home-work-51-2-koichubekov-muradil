import React from 'react';

var link = ["menu", "contact", "services", "about"];

const Header = (props) => {
    return (
        <div>
            <header className="header">
                <div className="logo">
                    <a href="#">{props.Logotext}</a>
                </div>
                <nav className="nav">
                    <a href="#">{link[0]}</a>
                    <a href="#">{link[1]}</a>
                    <a href="#">{link[2]}</a>
                    <a href="#">{link[3]}</a>
                </nav>
            </header>
        </div>
    );
};

export default Header;