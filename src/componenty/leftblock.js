import React from 'react';

var tovarTitle = ["Обувь", "Одежды", "Спортивные одежды", "Classic style"];

const Leftblock = (props) => {

    return (
            <div className="left_block">
                <h1 className="tov">Товары</h1>
                <div className="nav_bar">
                    <ul>
                        <a href="#" className="text">{tovarTitle[0]}</a>
                        <a href="#" className="text">{tovarTitle[1]}</a>
                        <a href="#" className="text">{tovarTitle[2]}</a>
                        <a href="#" className="text">{tovarTitle[3]}</a>
                    </ul>
                </div>
            </div>
    );
};

export default Leftblock;